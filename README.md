# StudiumPlus LaTeX Vorlage

Diese Vorlage für Ausarbeitungen im Rahmen eines dualen Studiums bei StudiumPlus orientiert sich an der [Richtlinie Wissenschaftliches Arbeiten](https://www.studiumplus.de/sp/images/_Downloads/Praxis/Richtlinie_Wiss_Arbeiten_Fassung_Okt2019.pdf).
